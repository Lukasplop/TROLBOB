import yaml
import io


test_boite = """
-   Contenus: ["Coucou", "Hibou"]
    Ouverte: True
    Capacite: 42
-   Contenus: []
    Ouverte: False
    Capacite: None
"""

stream = io.StringIO(test_boite)
test = yaml.load(stream)
test
print(test)
