class Box:
    def __init__(self, contenus = [], ouverte = True, capacite = None, kle = None):
        self._contenus = contenus
        self.ouvert = ouverte
        self.capacity_totale = capacite
        self.restant = self.capacity_totale
        self.key = kle;
        self.t = Thing(contenus, "boite", [self._contenus, self.ouvert, self.capacity_totale,self.restant,self.key])

    def add(self, trucaajouter):
        self._contenus.append(trucaajouter)

    def vide_ou_pas(self):
        return len(self._contenus) == 0

    def contient(self,truc):
        for element in self._contenus:
            if element == truc:
                return True
        return False

    def supprime(self, truc):
        self._contenus.remove(truc)

    def est_ouverte(self):
        return self.ouvert

    def fermer(self):
        self.ouvert = False

    def ouvrir(self):
        self.ouvert = True

    def regarder(self):
        if self.ouvert:
            print("Ta boite contient : " + ", ".join(self._contenus))
        else:
            print("Ta boite est fermée")

    def set_capacity(self, nombre):
        self.capacity_totale = nombre

    def get_capacity(self):
        return self.capacity_totale

    def a_de_la_place_pour(self, truc):
        if self.ouvert == True and (self.restant >= truc.volume or self.capacity_totale == None):
            self.add(truc)
            self.restant -= truc.volume
            print("Ajouté")
            return True
        else:
            if self.ouvert == False:
                print("Boite fermée")
                return False
            else:
                print("Pas assez de place !")
                return False

    def trouver(self, truc):
        if not self.ouvert:
            return None
        for objet in self._contenus:
            if objet == truc:
                return objet
        return None

    @staticmethod
    def from_yaml(data):
        c = data.get("Contenus")
        o = data.get("Ouverte")
        ct = data.get("Capacite")
        return Box(contenus = c, ouverte = o, capacite = ct)

    def setkey(self, truc):
        if self.key == None:
            self.key = truc
        else:
            print("Thu nheu peu chengé la kle dunheu bouate si el a desja une kle!")

    def ouvrir_avec(self, truc):
        if self.key == truc:
            self.ouvert = True
        else:
            print("Pas la bonne clé !")


class Thing:
    def __init__(self, a_volume, name = None, boite = None):
        self.volume = a_volume
        self.name = name
        self.boite = boite

    def a_volume(self):
        return self.volume

    def set_nom(self, nom):
        self.name = nom

    def affiche(self):
        print("Nom : " + self.name)

    def test_nom(self, nom):
        if self.name == nom:
            return True
        return False

    @staticmethod
    def from_yaml(data):
        v = data.get("Volume")
        n = data.get("Nom")
        return Box(a_volume = v, name = n)
